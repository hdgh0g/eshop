# eShop Rails App #

Простое веб-приложение на RoR, основа для интернет-магазина

### Требования к окружению ###

* Ruby 2.3.4 (для RVM имеются файлы конфигурации .ruby-version и .ruby-gemset)
* Все зависимости определены в Gemfile и Gemfile.lock
* MySQL 5.7:
    * Базы данных: eshop\_db, eshop\_dev, eshop\_test
    * Пользователь БД: eshop\_usr (без пароля)
* Для равзертывания использовать:
    * bundler (bundle install)
    * rake db:setup (в каталоге БД имеется файл seeds.rb с начальными данными)