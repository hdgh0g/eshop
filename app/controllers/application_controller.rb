class ApplicationController < ActionController::Base
  # Cart must be on all pages of WebApp
  include CurrentCart
  before_action :session_cart

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from ActionView::MissingTemplate do |exception|
    logger.error "#{exception}"
    redirect_to catalog_url, notice: "Страница не найдена"
  end
end
