class CartItemsController < ApplicationController
  include RedirectBack
  before_action :set_cart_item, only: [:show, :edit, :update, :destroy]

  # GET /cart_items
  # GET /cart_items.json
  def index
    @cart_items = CartItem.all
  end

  # GET /cart_items/1
  # GET /cart_items/1.json
  def show
  end

  # GET /cart_items/new
  def new
    @cart_item = CartItem.new
  end

  # GET /cart_items/1/edit
  def edit
  end

  # POST /cart_items
  # POST /cart_items.json
  def create
    item = Item.find(params[:item_id])
    @cart_item = @cart.change_quantity(item.id, true)

    respond_to do |format|
      if @cart_item.save
        format.html { redirect_back('Товар добавлен в корзину.') }
        format.json { render :show, status: :created, location: @cart_item }
      else
        format.html { render :new }
        format.json { render json: @cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cart_items/1
  # PATCH/PUT /cart_items/1.json
  def update
    respond_to do |format|
      if @cart_item.update(cart_item_params)
        format.html { redirect_to @cart_item, notice: 'Товар в корзине обновлен.' }
        format.json { render :show, status: :ok, location: @cart_item }
      else
        format.html { render :edit }
        format.json { render json: @cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cart_items/1
  # DELETE /cart_items/1.json
  def destroy
    item = Item.find(@cart_item.item_id)
    if params[:remove]
      @cart_item.destroy
    else
      @cart_item = @cart.change_quantity(item.id, false)
      @cart_item.save if @cart_item
    end

    respond_to do |format|
      if @cart.cart_items.empty?
        format.html { redirect_to catalog_url, notice: 'Корзина пуста.' }
      else
        format.html { redirect_back('Товар удален из корзины.') }
      end

      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_item_params
      params.require(:cart_item).permit(:cart_id, :quantity)
    end
end
