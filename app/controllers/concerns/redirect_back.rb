module RedirectBack
  extend ActiveSupport::Concern

  def redirect_back(notice = nil)
    redirect_to :back, notice: notice
  rescue ActionController::RedirectBackError
    redirect_to catalog_url, notice: notice
  end
end
