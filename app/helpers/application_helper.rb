module ApplicationHelper
  def hidden_div_if(condition, attribs = {}, &block)
    if condition
      attribs["style"] = "display: none"
    end
    content_tag("div", attribs, &block)
  end
end
