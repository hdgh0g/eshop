class Cart < ActiveRecord::Base
  include CartItemsSum
  has_many :cart_items, dependent: :destroy

  def change_quantity(item_id, add)
    current_item = cart_items.find_by(item_id: item_id)
    if current_item
      if add
        current_item.quantity += 1
      else
        if current_item.quantity == 1
          current_item.destroy
          return
        else
          current_item.quantity -= 1
        end
      end
    else
      if add
        current_item = cart_items.build(item_id: item_id)
      else
        return
      end
    end
    current_item
  end
end
