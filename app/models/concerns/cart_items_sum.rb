module CartItemsSum
  extend ActiveSupport::Concern

  def cart_items_sum
    cart_items.to_a.sum { |c_item| c_item.total_price }
  end
end
