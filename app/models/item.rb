class Item < ActiveRecord::Base
  has_many :cart_items

  before_destroy :ensure_not_referenced_by_any_cart_item

  validates :name, :desc, :img_url, presence: true
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  validates :name, uniqueness: true
  validates :img_url, allow_blank: true, format: {
    with: %r{\.(gif|jpg|jpeg|png)\Z}i,
    message: 'Allowed formats: GIF, JPG, PNG'
            # Разрешенные форматы изображений: GIF, JPG и PNG
  }

  private
    def ensure_not_referenced_by_any_cart_item
      if cart_items.empty?
        return true
      else
        errors.add(:base, 'В корзине есть товары!')
        return false
      end
    end
end
