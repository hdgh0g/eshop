class Order < ActiveRecord::Base
  include CartItemsSum
  has_many :cart_items, dependent: :destroy
  before_save :default_values

  def add_cart_items_from_cart(cart)
    cart.cart_items.each do |item|
      item.cart_id = nil
      cart_items << item
    end
  end

  def default_values
    self.num ||= ord_num_generator
  end

  private
    def ord_num_generator
      # Start values
      prefix = "ORD-"
      start_number = prefix + "000000"

      # Increment order number
      max_number = Order.maximum(:num) || start_number
      next_number = max_number.gsub(prefix, '').to_i + 1
      prefix + "%.6d" % next_number
    end
end
