json.extract! item, :id, :name, :desc, :img_url, :price, :created_at, :updated_at
json.url item_url(item, format: :json)
