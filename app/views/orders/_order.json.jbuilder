json.extract! order, :id, :num, :created_at, :updated_at
json.url order_url(order, format: :json)
