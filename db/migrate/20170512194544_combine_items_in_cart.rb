class CombineItemsInCart < ActiveRecord::Migration
  def up
    # Схлопывание товаров в корзине
    Cart.all.each do |cart|
      # Получаем количество каждого товара в корзине
      sums = cart.cart_items.group(:item_id).sum(:quantity)

      sums.each do |item_id, quantity|
        if quantity > 1
          # Удаляем записи
          cart.cart_items.where(item_id: item_id).delete_all

          # Создаем одну запись с нужным кол-вом
          item = cart.cart_items.build(item_id: item_id)
          item.quantity = quantity
          item.save!
        end
      end
    end
  end

  def down
    CartItem.where("quantity > 1").each do |cart_item|
      # Создаем записи с тем же товаром, у которого кол-во больше 1-го
      cart_item.quantity.times do
        CartItem.create cart_id: cart_item.cart_id, item_id: cart_item.item_id, quantity: 1
      end

      # Удаляем исходную запись
      cart_item.destroy
    end
  end
end
