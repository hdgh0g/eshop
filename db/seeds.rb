# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Item.create!(name: "Товар №1", desc: "<p>Какой-то товар</p>", img_url: "item.png", price: 123.56)
Item.create!(name: "Товар №2", desc: "<p>Ещё какой-то товар</p>", img_url: "item.png", price: 135.79)
Item.create!(name: "Товар №3", desc: "<p>И ещё один товар</p>", img_url: "item.png", price: 199.99)
