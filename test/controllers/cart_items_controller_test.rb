require 'test_helper'

class CartItemsControllerTest < ActionController::TestCase
  setup do
    @cart_item = cart_items(:item_in_cart)
    session[:cart_id] = carts(:test_cart).id
  end

  test "should create cart_item in cart" do
    assert_difference('CartItem.count') do
      post :create, item_id: items(:one)
    end

    assert_redirected_to catalog_path
  end

  test "should create cart_item in cart via ajax" do
    assert_difference('CartItem.count') do
      xhr :post, :create, item_id: items(:one)
    end

    assert_response :success
  end

  test "should update cart_item" do
    patch :update, id: @cart_item, cart_item: { quantity: @cart_item.quantity + 1 }
    assert_redirected_to cart_item_path(assigns(:cart_item))
  end

  test "should destroy cart_item" do
    assert_difference('CartItem.count', -1) do
      delete :destroy, id: @cart_item, remove: true
    end

    assert_redirected_to catalog_path
  end
end
