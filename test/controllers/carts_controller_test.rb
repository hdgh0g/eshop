require 'test_helper'

class CartsControllerTest < ActionController::TestCase
  setup do
    @cart = carts(:one)
  end

  test "should create cart" do
    # Ожидается наличие 2-х корзин после создания, т.к. существование корзины сессии проверяется и объявляется
    # при обращении к любому ресурсу приложения
    assert_difference('Cart.count', 2) do
      post :create, cart: {  }
    end

    assert_redirected_to cart_path(assigns(:cart))
  end

  test "should redirect if session cart is empty" do
    session[:cart_id] = @cart.id
    get :show
    assert_redirected_to catalog_url
  end

  test "should show session cart if not empty" do
    c_item = CartItem.new
    c_item.build_cart
    c_item.item = items(:test_item)
    c_item.save!
    session[:cart_id] = c_item.cart.id
    get :show
    assert_response :success
  end

  test "should update cart" do
    patch :update, id: @cart, cart: {  }
    assert_redirected_to cart_path(assigns(:cart))
  end

  test "should destroy cart" do
    assert_difference('Cart.count', -1) do
      session[:cart_id] = @cart.id
      delete :destroy, id: @cart
    end

    assert_redirected_to catalog_path
  end
end
