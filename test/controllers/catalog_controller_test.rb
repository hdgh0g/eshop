require 'test_helper'

class CatalogControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_select '#columns #sidebar a', minimum: 4
    assert_select '#content .entry', 3
    assert_select 'h3', 'Тестовый товар'
    assert_select '.price', /[,\d]+\.\d\d руб\./
  end

  test "markup needed for catalog.coffee is in place" do
    get :index
    assert_select '.catalog .entry > img', 3
    assert_select '.entry input[type=submit]', 3
  end
end
