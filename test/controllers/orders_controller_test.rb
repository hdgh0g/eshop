require 'test_helper'

class OrdersControllerTest < ActionController::TestCase
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders)
  end

  test "requires item in cart" do
    get :new
    assert_redirected_to catalog_path
    assert_equal flash[:notice], 'Корзина пуста'
  end

  test "should get new" do
    c_item = CartItem.new
    c_item.build_cart
    c_item.item = items(:test_item)
    c_item.save!
    session[:cart_id] = c_item.cart.id
    get :new
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post :create, order: {}
    end

    assert_redirected_to catalog_path
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete :destroy, id: @order
    end

    assert_redirected_to orders_path
  end
end
