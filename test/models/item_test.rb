require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  fixtures :items
  test "item attributes must not be empty" do
    # свойства товара не должны оставаться пустыми
    item = Item.new
    assert item.invalid?
    assert item.errors[:name].any?
    assert item.errors[:desc].any?
    assert item.errors[:price].any?
    assert item.errors[:img_url].any?
  end

  test "item price must be positive" do
    # цена товара должна быть положительной
    item = Item.new(
      name: "Test Item",
      desc: "ddd",
      img_url: "iii.jpg"
    )
    item.price = -1

    assert item.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
    # должна быть больше или равна 0.01
      item.errors[:price]

    item.price = 0
    assert item.invalid?

    assert_equal ["must be greater than or equal to 0.01"],
      item.errors[:price]
    item.price = 1
    assert item.valid?
  end

  def new_item(img_url)
    Item.new(
      name: "Test Item",
      desc: "ddd",
      price: 1,
      img_url: img_url
    )
  end

  test "image url" do
    # url
    ok = %w{ iii.gif iii.jpg iii.jpeg iii.png III.JPG III.Jpg http://a.b.c/x/y/z/iii.gif }
    bad = %w{ iii.doc iii.gif/other iii.jpg.more }

    ok.each do |url|
      assert new_item(url).valid?, "#{url} shouldn't be invalid"
    end

    bad.each do |url|
      assert new_item(url).invalid?, "#{url} shouldn't be valid"
    end
  end

  test "item is not valid without a unique title" do
    # не допускаются одинаковые наименования товаров
    item = Item.new(
      name: items(:test_item).name,
      desc: "ddd",
      img_url: "iii.jpg"
    )

    assert item.invalid?

    assert_equal ["has already been taken"], item.errors[:name]
  end
end
